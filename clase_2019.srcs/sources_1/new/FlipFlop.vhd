-- @file FlipFLop.vhd
-- @author AntoMota
-- @brief Flipflop demo

-- Libraries
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.std_logic_arith.all; -- Arithmetic functions
use ieee.std_logic_unsigned.all; -- Unsigned values

entity FLIPFLOP is
    port (
        CE, CLK: in std_logic;
        D: in std_logic_vector(3 downto 0);
        Q: out std_logic_vector(3 downto 0)
    
    );
    attribute IOB : string;
    attribute IOB of Q: signal is "false";

-- true use FF salida, false use FF slice
-- whithout attribute wherever
end  FLIPFLOP;

architecture Behavioral of FLIPFLOP is

begin
    process(CLK) begin
        if rising_edge(CLK) then
            if CE = '0' then
                Q <= D;
            else
                Q <= "0000";
            end if;
        end if;
    end process;
end Behavioral;
